package com.example.lab03.data;

import com.example.lab03.data.model.LoggedInUser;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {
    private static final String[] DUMMY_CREDENTIALS = new String[] {
            "pj42650@zut.edu.pl:123456", "jpolrolniczak@email.pl:654321" };

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
            for (String credential : DUMMY_CREDENTIALS)
            {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(username))
                {
                    // Account exists, return true if the password matches.
                    if (pieces[1].equals(password))
                    {
                        LoggedInUser testUser =
                                new LoggedInUser(
                                        java.util.UUID.randomUUID().toString(),
                                        "Joanna Półrolniczak");
                        return new Result.Success<>(testUser);
                    }
                    else
                        throw new Exception("Niepoprawne hasło");
                }
            }
            throw new Exception("Próba utworzenia konta: " + username );
        }
        catch (Exception e) {
            return new Result.Error(new IOException(e.getMessage()));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}